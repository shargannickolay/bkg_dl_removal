import os
import shutil
import glob
from pathlib import Path
import cv2


out_path = Path("./conv_hull_out")
mask_path = Path("./conv_hull_mask")

out_filennames = os.listdir(out_path)
maks_filennames = os.listdir(mask_path)


for filename in maks_filennames:
    if not filename in out_filennames:
        os.remove(mask_path / filename)