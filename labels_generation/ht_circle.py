import os
import numpy as np
import cv2
import tqdm
from pathlib import Path
import matplotlib.pyplot as plt


data_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamonds/round")

img_names = os.listdir(data_path)
img_names.sort()

os.makedirs("./hough_out/", exist_ok=True)
os.makedirs("./hough_mask/", exist_ok=True)

for img_name in tqdm.tqdm(img_names[:20]):
    img = cv2.imread(str(data_path / img_name))
    edges = cv2.Canny(img, 0, 255)

    min_rad = int(min(img.shape[0], img.shape[1]) / 20)
    max_rad = int(max(img.shape[0], img.shape[1]) / 2)

    circles = cv2.HoughCircles(edges, cv2.HOUGH_GRADIENT, 1, 20,
                               param1=50, param2=30, minRadius=min_rad, maxRadius=max_rad)

    img_mask = np.zeros((img.shape[0], img.shape[1]))

    try:
        circles = np.uint16(np.around(circles))
        for i in circles[0, :1]:
            cv2.circle(img, (i[0], i[1]), i[2], (0, 255, 0), 2)
            cv2.circle(img_mask, (i[0], i[1]), i[2], (255, 255, 255), -1)

        cv2.imwrite("./hough_out/" + img_name, img)
        cv2.imwrite("./hough_mask/" + img_name, img_mask)

    except:
        cv2.imwrite("./hough_out/" + img_name, img)
        cv2.imwrite("./hough_mask/" + img_name, img_mask)