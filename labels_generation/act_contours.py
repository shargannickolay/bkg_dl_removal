
import numpy as np
import matplotlib.pyplot as plt
from skimage.color import rgb2gray
from skimage import data
from skimage.filters import gaussian
from skimage.segmentation import active_contour
import cv2
from pathlib import Path
import os
import tqdm


data_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamonds/round")

os.makedirs("./act_contours_mask/", exist_ok=True)
os.makedirs("./act_contours/", exist_ok=True)
target_img_size = 500

img_names = os.listdir(data_path)

for img_name in tqdm.tqdm(img_names[:10]):

    print(str(data_path / img_name))
    img_orig = cv2.imread(str(data_path / img_name))
    img = cv2.resize(img_orig, (target_img_size, target_img_size), cv2.INTER_CUBIC)
    #         s = np.linspace(0, 2 * np.pi, 400)
    #         x = int(img.shape[1] / 2) + int(img.shape[1] / 2 - 3) * np.cos(s)
    #         y = int(img.shape[0] / 2) + int(img.shape[0] / 2 - 3) * np.sin(s)
    #         init = np.array([x, y]).T
    init_list = []

    for xx in range(0, img.shape[0], 3):
        init_list.append([xx, 0])
    for yy in range(0, img.shape[1], 3):
        init_list.append([img.shape[0] - 1, yy])
    for xx in range(0, img.shape[0], 3):
        init_list.append([img.shape[0] - 1 - xx, img.shape[1] - 1])
    for yy in range(0, img.shape[1], 3):
        init_list.append([0, img.shape[1] - 1 - yy])

    init_rec = np.asarray(init_list)

    snake = active_contour(img, init_rec, alpha=0.25, beta=25, gamma=0.04)  # gamma - curvature; alpha - how tight

    fig, ax = plt.subplots(figsize=(7, 7))
    ax.imshow(img, cmap=plt.cm.gray)
    ax.plot(init_rec[:, 0], init_rec[:, 1], '--r', lw=1)
    ax.plot(snake[:, 0], snake[:, 1], '-b', lw=1)
    ax.set_xticks([]), ax.set_yticks([])
    ax.axis([0, img.shape[1], img.shape[0], 0])

    plt.savefig("./act_contours/" + img_name)

    snake = np.reshape(snake, (snake.shape[0], 1, snake.shape[1]))
    snake_int = snake.astype(np.int32)
    mask = np.zeros((img.shape[0], img.shape[1], 3))  # create a single channel 200x200 pixel black image
    cv2.fillPoly(mask, pts=[snake_int], color=(255, 255, 255))
    mask = cv2.resize(mask, (img_orig.shape[0], img_orig.shape[1]))

    cv2.imwrite("./act_contours_mask/" + img_name, mask)
