import os
import shutil
from pathlib import Path


data_path = Path("./masks")

tr_path = Path("./masks_tr")
val_path = Path("./masks_val")

val_part = 0.15

os.makedirs(str(tr_path), exist_ok=True)
os.makedirs(str(val_path), exist_ok=True)

img_names = os.listdir(str(data_path))

val_num = int(len(img_names) * val_part)

for img_name in img_names[:val_num]:
    shutil.copy(data_path / img_name, val_path / img_name)

for img_name in img_names[val_num:]:
    shutil.copy(data_path / img_name, tr_path / img_name)