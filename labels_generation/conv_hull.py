import os
import tqdm
import numpy as np
import cv2
from pathlib import Path
import matplotlib.pyplot as plt

from skimage.morphology import convex_hull_image
from skimage import data, img_as_float
from skimage.util import invert


data_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamonds/round")

os.makedirs("./conv_hull_mask/", exist_ok=True)
os.makedirs("./conv_hull_out/", exist_ok=True)

img_names = os.listdir(data_path)
img_names.sort()

target_img_size = 500
min_area = 200

for img_name in tqdm.tqdm(img_names[:10]):
    print(str(data_path / img_name))
    img = cv2.imread(str(data_path / img_name))
    img = cv2.resize(img, (target_img_size, target_img_size), cv2.INTER_CUBIC)

    edges = cv2.Canny(img, 0, 255)

    chull = convex_hull_image(edges)
    chull_int = chull.astype(np.uint8)
    chull_int[chull_int!=0] = 255

    cv2.imwrite("./conv_hull_mask/" + img_name, chull_int)

    _, contours, _ = cv2.findContours(chull_int, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    for contour in contours:
        area = cv2.contourArea(contour)

    if area > min_area:
        cv2.drawContours(img, contour, -1, (0, 255, 0), 2)

    cv2.imwrite("./conv_hull_out/" + img_name, img)
