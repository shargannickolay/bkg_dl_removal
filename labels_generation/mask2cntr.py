import os
import numpy as np
import matplotlib.pyplot as plt
import cv2
from pathlib import Path

mask_path = Path("./conv_hull_mask")
mask_cntr_path = Path("./conv_hull_mask_cntr")

os.makedirs(mask_cntr_path, exist_ok=True)

img_names = os.listdir(mask_path)

for img_name in img_names:
    mask = cv2.imread(str(mask_path / img_name))
    eroded = cv2.erode(mask, np.ones((5, 5)))
    mask_cntr = cv2.subtract(mask, eroded)
    cv2.imwrite(str(mask_cntr_path / img_name), mask_cntr)