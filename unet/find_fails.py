
from PIL import Image
import os
import tqdm
import numpy as np
import glob
from pathlib import Path
from losses import mean_iou, bce_dice_loss, dice_coef
from keras.models import load_model
from keras import backend as K
from models import get_unet_mobilenet
import cv2
from skimage.morphology import convex_hull_image

import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)


def pred(weights_path, img_size, test_path, alpha_path):
    
    model = get_unet_mobilenet((img_size, img_size, 3), alpha=1.0)
    if not weights_path is None:
        model.load_weights(weights_path)
    model.compile(optimizer='adam', loss=bce_dice_loss, metrics=[mean_iou])

    os.makedirs(alpha_path, exist_ok=True)

    img_pathes = glob.glob(str(test_path / "*"))

    for idx, img_path in tqdm.tqdm(enumerate(img_pathes)):
        img_name = os.path.basename(img_path)
        if not img_name.endswith(".png"):
            continue
        img_orig = Image.open(img_path).convert('RGB')
        img = img_orig.resize((img_size, img_size), Image.ANTIALIAS)
        img = np.array(img)
        img = np.reshape(img, (1, img_size, img_size, 3))

        preds = model.predict(img, verbose=1)

        mask = preds[0]
        mask *= 255.

        thresh = 245
        mask[mask<thresh] = 0
        mask[mask >= thresh] = 255

        mask_h = convex_hull_image(mask[:, :, 0].astype(np.uint8))

        dice = K.eval(dice_coef(mask_h.astype(np.float32), mask[:, :, 0].astype(np.float32)))

        if dice > 0.99:
            continue
        print("dice less 0.99")

        img_orig = np.array(img_orig)

        mask = cv2.resize(mask, (img_orig.shape[1], img_orig.shape[0]))
        mask_3 = np.zeros((img_orig.shape[0], img_orig.shape[1], 3))
        mask_3[:, :, 0] = mask
        mask_3[:, :, 1] = mask
        mask_3[:, :, 2] = mask
        stacked = np.concatenate((img_orig, mask_3), axis=0)
        stacked = Image.fromarray(np.uint8(stacked))
        stacked.save(os.path.join(alpha_path, img_name))


def main():
    prev_weights_path = "./models/31_unet_mb_diamseg__04_0.9867_0.9915.h5"
    img_size = 512

    shapes = ['princess', 'oval', 'radiant', 'emerald', 'pear', 'marquise', 'cushion', 'asscher', 'round', 'heart']

    for shape in shapes[:-2]:
        test_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamonds/" + shape)
        alpha_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamond_data/alpha_3unet_fails/" +
                          shape)
        pred(prev_weights_path, img_size, test_path, alpha_path)


if __name__ == "__main__":
    main()

