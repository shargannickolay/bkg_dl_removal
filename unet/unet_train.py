
from PIL import Image
import os
import cv2
import numpy as np
from pathlib import Path
import glob
import tqdm
from generator import generator
from losses import mean_iou, bce_dice_loss, dice_coef
from models import get_unet_resnet, get_unet_mobilenet
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint, TensorBoard, ReduceLROnPlateau, EarlyStopping
from imgaug import augmenters as iaa
import imgaug as ia


import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)


img_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamonds")


seq = iaa.Sequential([
    iaa.Crop(px=(0, 16)),
    iaa.Fliplr(0.5),
    iaa.Flipud(0.5),
    iaa.GaussianBlur(sigma=(0.0, 1.0))
])


def data_generator(masks_pathes, batch_size, input_shape, ch_size=3, is_aug=True):
    img_num = len(masks_pathes)
    x_batch = np.zeros((batch_size, input_shape[0], input_shape[1], ch_size))
    y_batch = np.zeros((batch_size, input_shape[0], input_shape[1], 1))

    while True:
        for batch_idx, img_idx in enumerate(np.random.randint(img_num, size=batch_size)):
            mask = Image.open(masks_pathes[img_idx]).convert('L')
            mask = mask.resize((img_size, img_size), Image.ANTIALIAS)
            mask = np.reshape(mask, (input_shape[0], input_shape[1], 1))
            mask = np.asarray(mask)
            mask = mask.astype(np.bool)
            # mask[mask!=0] = 1

            shape = os.path.basename(os.path.dirname(masks_pathes[img_idx]))
            img_name = os.path.basename(masks_pathes[img_idx])
            img = Image.open(str(img_path / shape / img_name))
            img = img.resize((img_size, img_size), Image.ANTIALIAS).convert('RGB')
            img = np.asarray(img)

            if is_aug:
                segmap = ia.SegmentationMapOnImage(mask.astype(np.bool), shape=img.shape, nb_classes=1 + 1)
                seq_det = seq.to_deterministic()
                img = seq_det.augment_image(img)
                mask = seq_det.augment_segmentation_maps([segmap])[0].get_arr_int()
                mask = np.reshape(mask, (input_shape[0], input_shape[1], 1))
            # print(mask.shape)
            # print(np.unique(mask))

            y_batch[batch_idx, :, :, :] = mask
            x_batch[batch_idx, :, :, :] = img

        x_batch = x_batch.astype('float32')
        y_batch = y_batch.astype('float32')

        yield x_batch, y_batch


def train(prev_weights_path, epoch_num, batch_size, img_size, train_path):
    mask_pathes_tr = glob.glob("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamond_data/masks_tr/*/*")
    mask_pathes_val = glob.glob("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamond_data/masks_val/*/*")
    train_generator = data_generator(mask_pathes_tr, batch_size, (img_size, img_size, 3))
    val_generator = data_generator(mask_pathes_val, batch_size, (img_size, img_size, 3), is_aug=False)

    model = get_unet_mobilenet((img_size, img_size, 3), alpha=1.0)

    model = get_unet_mobilenet((img_size, img_size, 3), alpha=1.0)
    if not prev_weights_path is None:
        model.load_weights(prev_weights_path)
    model.compile(optimizer='adam', loss=bce_dice_loss, metrics=[mean_iou, dice_coef])

    if not os.path.isdir("./models"):
        os.mkdir("./models")
    checkpointer = ModelCheckpoint('./models/7_unet_mb100_diamseg__{epoch:02d}_{val_mean_iou:.4f}_{val_dice_coef:.4f}.h5', monitor='val_dice_coef',
                                   verbose=1, save_best_only=True, mode='max')
    tensorboard = TensorBoard(log_dir='./logs')
    reduce_lr = ReduceLROnPlateau(monitor='val_dice_coef', mode='max', factor=0.2, patience=5, min_lr=0.00001, verbose=1)
    early_stopping = EarlyStopping(monitor='val_dice_coef', mode='max', patience=15, verbose=1)
    callbacks_list = [checkpointer, tensorboard, reduce_lr, early_stopping]


    model.fit_generator(train_generator,
                        steps_per_epoch=len(mask_pathes_tr) // batch_size,
                        epochs=epoch_num,
                        validation_data=val_generator,
                        validation_steps=len(mask_pathes_val) // batch_size,
                        callbacks=callbacks_list
                        )


if __name__ == "__main__":
    prev_weights_path = None
    img_size = 512
    batch_size = 16
    epoch_num = 40
    train_path = "../"
    train(prev_weights_path, epoch_num, batch_size, img_size, train_path)
