
from PIL import Image
import os
import tqdm
import numpy as np
import glob
from pathlib import Path
from losses import mean_iou, bce_dice_loss
from keras.models import load_model
from models import get_unet_mobilenet
import cv2

import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)


def make_df_masks(test_path, img_size):
    test_ids = os.listdir(test_path)
    x_test = np.zeros((len(test_ids), img_size, img_size, 3), dtype=np.uint8)
    for i, id_ in enumerate(test_ids):
        if not id_.endswith(".png"):
            continue

        img = Image.open(os.path.join(test_path, id_)).convert('RGB')
        img = img.resize((img_size, img_size), Image.ANTIALIAS)
        img = np.array(img)
        x_test[i] = img

    return x_test, test_ids


def pred(weights_path, img_size, test_path, pred_path, alpha_path):
    x_test, test_ids = make_df_masks(test_path, img_size)

    model = get_unet_mobilenet((img_size, img_size, 3), alpha=0.5)
    if not weights_path is None:
        model.load_weights(weights_path)
    model.compile(optimizer='adam', loss=bce_dice_loss, metrics=[mean_iou])

    preds = model.predict(x_test, verbose=1)

    os.makedirs(pred_path, exist_ok=True)
    os.makedirs(alpha_path, exist_ok=True)

    for idx, test_id_ in tqdm.tqdm(enumerate(test_ids)):
        shape = os.path.basename(os.path.dirname(test_id_))
        test_id = os.path.basename(test_id_)
        if not test_id.endswith(".png"):
            continue
        img_orig = Image.open(os.path.join(test_path, shape, test_id)).convert('RGB')
        img = img_orig.resize((img_size, img_size), Image.ANTIALIAS)
        img = np.array(img)
        img_orig = np.array(img_orig)
        mask = preds[idx]
        mask *= 255.

        mask = cv2.resize(mask, (img_orig.shape[1], img_orig.shape[0]))
        r_channel, g_channel, b_channel = cv2.split(img_orig)
        a_channel = np.asarray(mask, dtype=np.uint8)
        img_RGBA = cv2.merge((r_channel, g_channel, b_channel, a_channel))

        cv2.imwrite(os.path.join(str(alpha_path), test_id), img_RGBA)

        # mask = [mask, mask, mask]
        # mask = np.asarray(mask)
        # mask = np.reshape(mask, (3, img_size, img_size))
        # mask = np.transpose(mask, (1, 2, 0))
        # print(mask.shape)
        # print(img.shape)
        # stacked = np.concatenate((img, mask), axis=0)
        # stacked = Image.fromarray(np.uint8(stacked))
        # stacked.save(os.path.join(pred_path, test_id))


def main():
    prev_weights_path = "./models/6_unet_mb50_diamseg__30_0.9011_0.8875.h5"
    img_size = 224
    test_path = Path("./data/test")
    pred_path = Path("./data/pred")
    alpha_path = Path("./data/alpha")
    pred(prev_weights_path, img_size, test_path, pred_path, alpha_path)


if __name__ == "__main__":
    main()

