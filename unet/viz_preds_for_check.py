
from PIL import Image
import os
import tqdm
import numpy as np
import glob
from pathlib import Path
from losses import mean_iou, bce_dice_loss, dice_coef
from keras.models import load_model
from keras import backend as K
from models import get_unet_mobilenet
import cv2
from skimage.morphology import convex_hull_image

import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)


min_area = 200
data_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamonds")

def make_df_masks(filepathes, img_size):
    x_test = np.zeros((len(filepathes), img_size, img_size, 3), dtype=np.uint8)
    for i, filepath in tqdm.tqdm(enumerate(filepathes)):
        if not filepath.endswith(".png"):
            continue

        img = Image.open(filepath).convert('RGB')
        img = img.resize((img_size, img_size), Image.ANTIALIAS)
        img = np.array(img)
        x_test[i] = img

    return x_test


def visualize(weights_path, img_size, filepathes, out_path):
    # x_test = make_df_masks(filepathes, img_size)

    model = get_unet_mobilenet((img_size, img_size, 3), alpha=1.0)
    if not weights_path is None:
        model.load_weights(weights_path)
    model.compile(optimizer='adam', loss=bce_dice_loss, metrics=[mean_iou])

    # preds = model.predict(x_test, verbose=1)

    os.makedirs(out_path, exist_ok=True)

    for idx, img_path in tqdm.tqdm(enumerate(filepathes)):

        mask_label = cv2.imread(img_path, 0)
        mask_label = mask_label / 255
        mask_label = np.reshape(mask_label, (mask_label.shape[0], mask_label.shape[1], 1))

        shape = os.path.basename(os.path.dirname(img_path))
        img_name = os.path.basename(img_path)
        if not img_name.endswith(".png"):
            continue
        img_orig = Image.open(os.path.join(data_path, shape, img_name)).convert('RGB')
        img = img_orig.resize((img_size, img_size), Image.ANTIALIAS)
        img = np.array(img)
        img = np.reshape(img, (1, img_size, img_size, 3))
        preds = model.predict(img)
        img_orig = np.array(img_orig)
        mask_pred = preds[0]
        mask_pred *= 255.

        thresh = 245
        mask_pred[mask_pred<thresh] = 0
        mask_pred[mask_pred >= thresh] = 255

        mask_pred = mask_pred / 255

        mask_pred = cv2.resize(mask_pred, (img_orig.shape[1], img_orig.shape[0]))
        mask_label = cv2.resize(mask_label, (img_orig.shape[1], img_orig.shape[0]))

        dice = K.eval(dice_coef(mask_label.astype(np.float32), mask_pred))

        _, contours, _ = cv2.findContours(mask_pred.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        for contour in contours:
            area = cv2.contourArea(contour)

            if area > min_area:
                cv2.drawContours(img_orig, contour, -1, (0, 255, 0), 2)

        _, contours, _ = cv2.findContours(mask_label.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        for contour in contours:
            area = cv2.contourArea(contour)

            if area > min_area:
                cv2.drawContours(img_orig, contour, -1, (0, 0, 255), 2)

        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(img_orig, str(dice), (30, 30), font, 1, (255, 255, 255), 2, cv2.LINE_AA)

        # merged = np.zeros((2 * img_orig.shape[0], 2 * img_orig.shape[1], 3), dtype=np.uint8)
        # merged[]



        cv2.imwrite(os.path.join(str(out_path), img_name), img_orig)


def main():
    weights_path = "./models/31_unet_mb_diamseg__04_0.9867_0.9915.h5"
    img_size = 512
    filenpathes_to_check = glob.glob("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamond_data/masks_tr/*/*")
    out_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamond_data/viz_check_tr_2")
    visualize(weights_path, img_size, filenpathes_to_check[:5], out_path)


if __name__ == "__main__":
    main()

