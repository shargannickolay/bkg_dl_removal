
from PIL import Image
import os
import tqdm
import numpy as np
import glob
from pathlib import Path
from losses import mean_iou, bce_dice_loss
from keras.models import load_model
from models import get_unet_mobilenet
import cv2
from skimage.morphology import convex_hull_image

import tensorflow as tf

config = tf.ConfigProto()
config.gpu_options.allow_growth=True
sess = tf.Session(config=config)


min_area = 200

def make_df_masks(test_path, img_size):
    # test_ids = os.listdir(test_path)[300:600]
    test_ids = os.listdir(test_path)
    x_test = np.zeros((len(test_ids), img_size, img_size, 3), dtype=np.uint8)
    for i, id_ in tqdm.tqdm(enumerate(test_ids)):
        if not id_.endswith(".png"):
            continue

        img = Image.open(os.path.join(test_path, id_)).convert('RGB')
        img = img.resize((img_size, img_size), Image.ANTIALIAS)
        img = np.array(img)
        x_test[i] = img

    return x_test, test_ids


def pred(weights_path, img_size, test_path, alpha_path):
    x_test, img_paths = make_df_masks(test_path, img_size)

    model = get_unet_mobilenet((img_size, img_size, 3), alpha=1.0)
    if not weights_path is None:
        model.load_weights(weights_path)
    model.compile(optimizer='adam', loss=bce_dice_loss, metrics=[mean_iou])

    preds = model.predict(x_test, verbose=1)

    os.makedirs(alpha_path, exist_ok=True)

    for idx, img_path in tqdm.tqdm(enumerate(img_paths)):
        shape = os.path.basename(os.path.dirname(img_path))
        img_name = os.path.basename(img_path)
        if not img_name.endswith(".png"):
            continue
        img_orig = Image.open(os.path.join(test_path, shape, img_name)).convert('RGB')
        img = img_orig.resize((img_size, img_size), Image.ANTIALIAS)
        img = np.array(img)
        img_orig = np.array(img_orig)
        mask = preds[idx]
        mask *= 255.

        thresh = 245
        mask[mask<thresh] = 0
        mask[mask >= thresh] = 255

        _, contours, _ = cv2.findContours(mask.astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        coef_x = img_orig.shape[1] / img_size
        coef_y = img_orig.shape[0] / img_size

        for contour in contours:
            area = cv2.contourArea(contour)

            contour[:, :, 0] = contour[:, :, 0] * coef_x
            contour[:, :, 1] = contour[:, :,  1] * coef_y

            if area > min_area:
                cv2.drawContours(img_orig, contour, -1, (0, 255, 0), 2)


        cv2.imwrite(os.path.join(str(alpha_path), img_name), img_orig)


def main():
    prev_weights_path = "./models/25_unet_mb_100_diamseg__02_0.9746_0.9848.h5"
    img_size = 224

    shapes = ['princess', 'oval', 'emerald', 'radiant', 'pear', 'marquise', 'cushion', 'asscher', 'round', 'heart']

    for shape in shapes[-1:]:
        test_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamonds/" + shape)
        alpha_path = Path("/media/nick/3d1b1d42-5da8-4fdf-934f-aec7826e0b3d6/Data/diamond_data/alpha_3unet_heart_cntr/" + shape)
        pred(prev_weights_path, img_size, test_path, alpha_path)


if __name__ == "__main__":
    main()

